﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableSpot : MonoBehaviour
{
    [SerializeField] string objName;
    public string GetName()
    {
        return objName;
    }
}
